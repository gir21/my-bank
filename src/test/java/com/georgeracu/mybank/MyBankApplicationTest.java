package com.georgeracu.mybank;

import com.georgeracu.mybank.bankaccount.BankAccountResource;
import com.georgeracu.mybank.transfer.resources.TransferResource;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.jersey.setup.JerseyEnvironment;
import io.dropwizard.setup.Environment;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Author georgicaracu
 */
public class MyBankApplicationTest {
    private final Environment environment = mock(Environment.class);
    private final JerseyEnvironment jersey = mock(JerseyEnvironment.class);
    private final MyBankApplication application = new MyBankApplication();
    private final MyBankConfiguration config = new MyBankConfiguration();

    @Before
    public void setup() {
        final DataSourceFactory dataSourceFactory = new DataSourceFactory();
        dataSourceFactory.setDriverClass("org.h2.Driver");
        dataSourceFactory.setUrl("jdbc:h2:mem:test-" + System.currentTimeMillis() + "?user=sa");
        dataSourceFactory.setInitialSize(1);
        config.setDataSourceFactory(dataSourceFactory);
        when(environment.jersey()).thenReturn(jersey);
    }

    @Ignore("need to wire in Hibernate Session Factory")
    @Test
    public void shouldWireAllResources() throws Exception {
        application.run(config, environment);
        verify(jersey).register(isA(TransferResource.class));
        verify(jersey).register(isA(BankAccountResource.class));
    }
}