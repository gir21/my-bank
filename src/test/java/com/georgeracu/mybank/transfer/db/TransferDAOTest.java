package com.georgeracu.mybank.transfer.db;


import com.georgeracu.mybank.transfer.core.entity.TransferEntity;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Author georgicaracu
 */
public class TransferDAOTest {
    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
            .addEntityClass(TransferEntity.class)
            .build();

    private TransferDAO dao;

    @Before
    public void setUp() {
        dao = new TransferDAO(daoTestRule.getSessionFactory());
    }

    @Test
    public void createTransferEntity() {
        TransferEntity defaultEntity = new TransferEntity("sender-id", "receiver-id", 100L);
        TransferEntity transfer = daoTestRule.inTransaction(() -> dao.save(defaultEntity));
        assertTrue(transfer.getId() > 0L);
        assertEquals(32, transfer.getExternalId().length());
        assertEquals(100L, transfer.getAmount());
        assertTrue(transfer.getCreatedAt().isBefore(ZonedDateTime.now()));
    }

    @Test
    public void shouldFindByExternalId() {
        TransferEntity defaultEntity = new TransferEntity("sender-id", "receiver-id", 100L);
        daoTestRule.inTransaction(() -> dao.save(defaultEntity));
        Optional<TransferEntity> maybeEntity = dao.findByExternalId(defaultEntity.getExternalId());
        assertTrue(maybeEntity.isPresent());
    }
}