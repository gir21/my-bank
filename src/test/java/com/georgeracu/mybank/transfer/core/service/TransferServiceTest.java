package com.georgeracu.mybank.transfer.core.service;

import com.georgeracu.mybank.bankaccount.core.service.BankAccountService;
import com.georgeracu.mybank.transfer.api.TransferResponse;
import com.georgeracu.mybank.transfer.core.entity.TransferEntity;
import com.georgeracu.mybank.transfer.db.TransferDAO;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Author georgicaracu
 */
public class TransferServiceTest {

    private static final String SENDER_ID = "sender-id";
    private static final String RECEIVER_ID = "receiver-id";
    private static final long AMOUNT = 100L;
    private TransferDAO mockedDAO = mock(TransferDAO.class);
    private BankAccountService mockedService = mock(BankAccountService.class);
    private TransferService service = new TransferService(mockedDAO, mockedService);

    @Test
    public void shouldCreateATransfer() {
        Optional<TransferResponse> maybeTransferId = service.createTransfer(SENDER_ID, RECEIVER_ID, AMOUNT);
        assertTrue(maybeTransferId.isPresent());
        assertThat(maybeTransferId.get().getExternalId().length(), is(32));
    }

    @Test
    public void shouldFindByExternalId() {
        TransferEntity defaultEntity = new TransferEntity("sender-id", "receiver-id", 100L);
        when(mockedDAO.findByExternalId(defaultEntity.getExternalId())).thenReturn(Optional.of(defaultEntity));
        Optional<TransferResponse> maybeEntity = service.findByExternalId(defaultEntity.getExternalId());
        assertTrue(maybeEntity.isPresent());
    }
}