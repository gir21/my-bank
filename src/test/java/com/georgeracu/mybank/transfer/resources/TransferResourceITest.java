package com.georgeracu.mybank.transfer.resources;

import com.georgeracu.mybank.IntegrationTest;
import com.georgeracu.mybank.MyBankApplication;
import com.georgeracu.mybank.MyBankConfiguration;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Author georgicaracu
 */
@Category(IntegrationTest.class)
@Ignore
public class TransferResourceITest {
    private static final String testConfigFile = "test-config.yml";
    // this is the default account that all new accounts can draw from
    private static final String MASTER_ACCOUNT_ID = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath(testConfigFile);

    @ClassRule
    public static final DropwizardAppRule<MyBankConfiguration> RULE = new DropwizardAppRule<>(
            MyBankApplication.class, CONFIG_PATH);

    @BeforeClass
    public static void migrateDatabase() throws Exception {
        RULE.getApplication().run("db", "migrate", ResourceHelpers.resourceFilePath(testConfigFile));
    }

    @AfterClass
    public static void tearDown() throws Exception {
        RULE.getApplication().run("db", "rollback", "--count", "3", ResourceHelpers.resourceFilePath(testConfigFile));
    }

    @Test
    public void shouldCreateATransfer() {
        String receiver = createBankAccount();
        Map<String, Object> request = createRequest(MASTER_ACCOUNT_ID, receiver, 100L);
        Response response = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/transfers").request().post(Entity.json(request));
        assertThat(response.getStatus(), is(201));
        assertNotNull(response.getLocation().getPath());
    }

    private Map<String, Object> createRequest(String sender, String receiver, long amount) {
        Map<String, Object> map = new HashMap<>();
        map.put("sender", sender);
        map.put("receiver", receiver);
        map.put("amount", amount);
        return map;
    }

//    @Test
//    public void shouldFindATransferByExternalId() {
//        String receiver = createBankAccount();
//        Map<String, Object> request = createRequest(MASTER_ACCOUNT_ID, receiver, 100L);
//        Response response = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/transfers").request().post(Entity.json(request));
//        assertThat(response.getStatus(), is(201));
//        assertNotNull(response.getLocation().getPath());
//        String externalId = response.getLocation().getPath().replace("/transfers/", "");
//        TransferResponse jsonResponse = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/transfers/" + externalId).request().get(TransferResponse.class);
//        assertNotNull(jsonResponse);
//        assertEquals(0, jsonResponse.getAmount());
//    }

    private String createBankAccount() {
        Response response = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/accounts").request().post(null);
        assertThat(response.getStatus(), is(201));
        return response.getLocation().getPath().replace("/accounts/", "");
    }
}
