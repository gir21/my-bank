package com.georgeracu.mybank.transfer.resources;


import com.georgeracu.mybank.transfer.api.TransferResponse;
import com.georgeracu.mybank.transfer.core.entity.TransferEntity;
import com.georgeracu.mybank.transfer.core.service.TransferService;
import io.dropwizard.jersey.validation.ValidationErrorMessage;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.hamcrest.core.Is;
import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Author georgicaracu
 */
public class TransferResourceTest {

    private static TransferService mockedService = mock(TransferService.class);

    @ClassRule
    public static final ResourceTestRule RULE = ResourceTestRule.builder()
            .addResource(new TransferResource(mockedService))
            .build();

    @After
    public void reset() {
        Mockito.reset(mockedService);
    }

    @Test
    public void shouldCreateATransfer() {
        String sender = "sender-id";
        String receiver = "receiver-id";
        long amount = 1000L;
        TransferEntity transferEntity = new TransferEntity("sender", "receiver", 100L);
        TransferResponse transferResponse = TransferResponse.from(transferEntity);
        when(mockedService.createTransfer(sender, receiver, amount)).thenReturn(Optional.of(transferResponse));
        Map<String, Object> createTransfer = getRequestMap("sender-id", "receiver-id", 1000L);
        Response response = RULE.target("/transfers").request().post(Entity.json(createTransfer));
        assertThat(response.getStatus(), is(201));
        assertThat(response.getLocation().getPath(), is("/transfers/" + transferEntity.getExternalId()));
    }

    @Test
    public void shouldReturn500WhenThereIsNoTransferCreated() {
        String sender = "sender-id";
        String receiver = "receiver-id";
        long amount = 1000L;
        when(mockedService.createTransfer(sender, receiver, amount)).thenReturn(Optional.empty());
        Map<String, Object> createTransfer = getRequestMap(sender, receiver, amount);
        Response response = RULE.target("/transfers").request().post(Entity.json(createTransfer));
        assertThat(response.getStatus(), is(500));
    }

    @Test
    public void shouldReturn422WhenMissingSenderId() {
        String sender = null;
        String receiver = "receiver-id";
        long amount = 1000L;
        Map<String, Object> createTransfer = getRequestMap(sender, receiver, amount);
        Response response = RULE.target("/transfers").request().post(Entity.json(createTransfer));
        assertThat(response.getStatus(), is(422));
        ValidationErrorMessage msg = response.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors().size(), Is.is(1));
        assertThat(msg.getErrors(), hasItems("sender may not be null"));
    }

    @Test
    public void shouldReturn422WhenMissingReceiverId() {
        String sender = "sender-id";
        String receiver = null;
        long amount = 1000L;
        Map<String, Object> createTransfer = getRequestMap(sender, receiver, amount);
        Response response = RULE.target("/transfers").request().post(Entity.json(createTransfer));
        assertThat(response.getStatus(), is(422));
        ValidationErrorMessage msg = response.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors().size(), Is.is(1));
        assertThat(msg.getErrors(), hasItems("receiver may not be null"));
    }

    @Test
    public void shouldReturn422WhenZeroAmount() {
        Map<String, Object> createTransfer = new HashMap<>();
        createTransfer.put("sender", "sender-id");
        createTransfer.put("receiver", "receiver-id");
        createTransfer.put("amount", 0L);
        Response response = RULE.target("/transfers").request().post(Entity.json(createTransfer));
        assertThat(response.getStatus(), is(422));
        ValidationErrorMessage msg = response.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors().size(), Is.is(1));
        assertThat(msg.getErrors(), hasItems("amount must be greater than or equal to 1"));
    }

    @Test
    public void shouldReturn422WhenNegativeAmount() {
        Map<String, Object> createTransfer = new HashMap<>();
        createTransfer.put("sender", "sender-id");
        createTransfer.put("receiver", "receiver-id");
        createTransfer.put("amount", -1L);
        Response response = RULE.target("/transfers").request().post(Entity.json(createTransfer));
        assertThat(response.getStatus(), is(422));
        ValidationErrorMessage msg = response.readEntity(ValidationErrorMessage.class);
        assertThat(msg.getErrors().size(), Is.is(1));
        assertThat(msg.getErrors(), hasItems("amount must be greater than or equal to 1"));
    }

    @Test
    public void shouldReturnEntityWhenGETById() {
        TransferEntity entity = new TransferEntity("sender-id", "receiver-id", 100L);
        TransferResponse fromEntity = TransferResponse.from(entity);
        when(mockedService.findByExternalId("abc")).thenReturn(Optional.of(fromEntity));
        Response response = RULE.target("/transfers/abc").request().get();
        assertThat(response.getStatus(), is(200));
        TransferResponse transferResponse = response.readEntity(TransferResponse.class);
        assertNotNull(transferResponse);
        assertThat(transferResponse.getExternalId(), is(entity.getExternalId()));
        assertThat(transferResponse.getSender(), is(entity.getSenderId()));
        assertThat(transferResponse.getReceiver(), is(entity.getReceiverId()));
        assertThat(transferResponse.getAmount(), is(entity.getAmount()));
    }

    @Test
    public void shouldReturn404WhenNotFoundByExternalId() {
        when(mockedService.findByExternalId("abc")).thenReturn(Optional.empty());
        Response response = RULE.target("/transfers/abc").request().get();
        assertThat(response.getStatus(), is(404));
    }

    private Map<String, Object> getRequestMap(String sender, String receiver, long amount) {
        Map<String, Object> createTransfer = new HashMap<>();
        createTransfer.put("sender", sender);
        createTransfer.put("receiver", receiver);
        createTransfer.put("amount", amount);
        return createTransfer;
    }
}