package com.georgeracu.mybank;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Author georgicaracu
 */
public class Random32CharactersStringGeneratorTest {
    @Test
    public void shouldGenerate32CharactersString() {
        String next = Random32CharactersStringGenerator.next();
        assertEquals(32, next.length());
    }
}