package com.georgeracu.mybank.bankaccount.core.service;


import com.georgeracu.mybank.bankaccount.api.response.BankAccountResponse;
import com.georgeracu.mybank.bankaccount.core.entity.BankAccountEntity;
import com.georgeracu.mybank.bankaccount.core.exception.BankAccountNotFoundException;
import com.georgeracu.mybank.bankaccount.db.BankAccountDAO;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Author georgicaracu
 */
public class BankAccountServiceTest {
    private BankAccountDAO mockedDAO = mock(BankAccountDAO.class);
    private BankAccountService service;

    @Before
    public void setup() {
        service = new BankAccountService(mockedDAO);
    }

    @After
    public void reset() {
        Mockito.reset(mockedDAO);
    }

    @Test
    public void shouldTransferBetweenAccounts() {
        BankAccountEntity sender = BankAccountEntity.defaultAccount();
        sender.credit(150L);
        assertThat(sender.getBalance(), is(150L));
        BankAccountEntity receiver = BankAccountEntity.defaultAccount();
        assertThat(receiver.getBalance(), is(0L));
        when(mockedDAO.findByExternalId(sender.getExternalId())).thenReturn(Optional.of(sender));
        when(mockedDAO.findByExternalId(receiver.getExternalId())).thenReturn(Optional.of(receiver));
        service.transferFunds(sender.getExternalId(), receiver.getExternalId(), 100L);
        assertThat(sender.getBalance(), is(50L));
        assertThat(receiver.getBalance(), is(100L));
    }

    @Test(expected = BankAccountNotFoundException.class)
    public void shouldThrowExceptionWhenSenderNotFound() {
        BankAccountEntity sender = BankAccountEntity.defaultAccount();
        BankAccountEntity receiver = BankAccountEntity.defaultAccount();
        when(mockedDAO.findByExternalId(sender.getExternalId())).thenReturn(Optional.empty());
        service.transferFunds(sender.getExternalId(), receiver.getExternalId(), 100L);
    }

    @Test(expected = BankAccountNotFoundException.class)
    public void shouldThrowExceptionWhenReceiverNotFound() {
        BankAccountEntity sender = BankAccountEntity.defaultAccount();
        BankAccountEntity receiver = BankAccountEntity.defaultAccount();
        when(mockedDAO.findByExternalId(sender.getExternalId())).thenReturn(Optional.of(sender));
        when(mockedDAO.findByExternalId(receiver.getExternalId())).thenReturn(Optional.empty());
        service.transferFunds(sender.getExternalId(), receiver.getExternalId(), 100L);
    }

    @Test
    public void shouldCreate() {
        BankAccountEntity newAccount = BankAccountEntity.defaultAccount();
        when(mockedDAO.save(any(BankAccountEntity.class))).thenReturn(newAccount);
        Optional<BankAccountResponse> maybeId = service.create();
        assertTrue(maybeId.isPresent());
    }

    @Test
    public void shouldFindByExternalId() {
        BankAccountEntity newAccount = BankAccountEntity.defaultAccount();
        when(mockedDAO.findByExternalId(newAccount.getExternalId())).thenReturn(Optional.of(newAccount));
        Optional<BankAccountResponse> maybeResponse = service.findByExternalId(newAccount.getExternalId());
        assertTrue(maybeResponse.isPresent());
        BankAccountResponse jsonResponse = maybeResponse.get();
        assertThat(jsonResponse.getId(), is(newAccount.getExternalId()));
        assertThat(jsonResponse.getBalance(), is(newAccount.getBalance()));
        assertThat(jsonResponse.getCreatedAt().toLocalDateTime(), is(newAccount.getCreatedAt().toLocalDateTime()));
    }
}