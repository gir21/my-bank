package com.georgeracu.mybank.bankaccount.core.entity;

import com.georgeracu.mybank.bankaccount.core.exception.InsufficientFundsException;
import com.georgeracu.mybank.bankaccount.core.exception.InvalidAmountException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;


/**
 * Author georgicaracu
 */
public class BankAccountEntityTest {
    @Test
    public void shouldCreateDefaultEntityWithZeroFunds() {
        BankAccountEntity entity = BankAccountEntity.defaultAccount();
        assertEquals(0L, entity.getBalance());
    }

    @Test(expected = InvalidAmountException.class)
    public void shouldNotCreditNegativeAmounts() {
        BankAccountEntity entity = BankAccountEntity.defaultAccount();
        entity.credit(-1L);
    }

    @Test(expected = InvalidAmountException.class)
    public void shouldNotCreditZeroAmount() {
        BankAccountEntity entity = BankAccountEntity.defaultAccount();
        entity.credit(0L);
    }

    @Test(expected = InvalidAmountException.class)
    public void shouldNotDebitNegativeAmounts() {
        BankAccountEntity entity = BankAccountEntity.defaultAccount();
        entity.debit(-1L);
    }

    @Test(expected = InvalidAmountException.class)
    public void shouldNotDebitZeroAmount() {
        BankAccountEntity entity = BankAccountEntity.defaultAccount();
        entity.debit(0L);
    }

    @Test(expected = InsufficientFundsException.class)
    public void shouldNotDebitMoreThanAvailable() {
        BankAccountEntity entity = BankAccountEntity.defaultAccount();
        assertEquals(0L, entity.getBalance());
        entity.debit(10L);
    }

}