package com.georgeracu.mybank.bankaccount.db;

import com.georgeracu.mybank.bankaccount.core.entity.BankAccountEntity;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Author georgicaracu
 */
public class BankAccountDAOTest {
    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
            .addEntityClass(BankAccountEntity.class)
            .build();

    private BankAccountDAO dao;

    @Before
    public void setUp() {
        dao = new BankAccountDAO(daoTestRule.getSessionFactory());
    }

    @Test
    public void createBankAccountEntity() {
        BankAccountEntity account = daoTestRule.inTransaction(() -> dao.save(BankAccountEntity.defaultAccount()));
        assertTrue(account.getId() > 0L);
        assertEquals(32, account.getExternalId().length());
        assertEquals(0, account.getBalance());
        assertTrue(account.getCreatedAt().isBefore(ZonedDateTime.now()));
    }

    @Test
    public void shouldFindByExternalId() {
        BankAccountEntity newAccount = BankAccountEntity.defaultAccount();
        daoTestRule.inTransaction(() -> dao.save(newAccount));
        Optional<BankAccountEntity> maybeEntity = dao.findByExternalId(newAccount.getExternalId());
        assertTrue(maybeEntity.isPresent());
    }
}