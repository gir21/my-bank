package com.georgeracu.mybank.bankaccount.resources;


import com.georgeracu.mybank.bankaccount.BankAccountResource;
import com.georgeracu.mybank.bankaccount.api.response.BankAccountResponse;
import com.georgeracu.mybank.bankaccount.core.entity.BankAccountEntity;
import com.georgeracu.mybank.bankaccount.core.service.BankAccountService;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.ClassRule;
import org.junit.Test;
import org.mockito.Mockito;

import javax.ws.rs.core.Response;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Author georgicaracu
 */
public class BankAccountResourceTest {
    private static BankAccountService mockedService = mock(BankAccountService.class);

    @ClassRule
    public static final ResourceTestRule RULE = ResourceTestRule.builder()
            .addResource(new BankAccountResource(mockedService))
            .build();

    @After
    public void reset() {
        Mockito.reset(mockedService);
    }

    @Test
    public void shouldCreateAnAccount() {
        BankAccountResponse accountResponse = BankAccountResponse.from(BankAccountEntity.defaultAccount());
        when(mockedService.create()).thenReturn(Optional.of(accountResponse));
        Response response = RULE.target("/accounts").request().post(null);
        assertThat(response.getStatus(), is(201));
        assertThat(response.getLocation().getPath(), is("/accounts/" + accountResponse.getId()));
    }

    @Test
    public void shouldReturn500() {
        when(mockedService.create()).thenReturn(Optional.empty());
        Response response = RULE.target("/accounts").request().post(null);
        assertThat(response.getStatus(), is(500));
    }

    @Test
    public void shouldFindByExternalId() {
        BankAccountEntity entity = BankAccountEntity.defaultAccount();
        BankAccountResponse mockedResponse = BankAccountResponse.from(entity);
        when(mockedService.findByExternalId("some-id")).thenReturn(Optional.of(mockedResponse));
        Response response = RULE.target("/accounts/some-id").request().get();
        assertThat(response.getStatus(), is(200));
        BankAccountResponse jsonResponse = response.readEntity(BankAccountResponse.class);
        assertNotNull(jsonResponse);
        assertThat(jsonResponse.getId(), is(entity.getExternalId()));
        assertThat(jsonResponse.getBalance(), is(entity.getBalance()));
        assertEquals(jsonResponse.getCreatedAt().toLocalDateTime(), entity.getCreatedAt().toLocalDateTime());
    }

    @Test
    public void shouldReturn404WhenNotFoundByExternalId() {
        when(mockedService.findByExternalId("some-id")).thenReturn(Optional.empty());
        Response response = RULE.target("/accounts/some-id").request().get();
        assertThat(response.getStatus(), is(404));
    }
}
