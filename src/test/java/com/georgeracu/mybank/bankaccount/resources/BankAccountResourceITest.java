package com.georgeracu.mybank.bankaccount.resources;

import com.georgeracu.mybank.IntegrationTest;
import com.georgeracu.mybank.MyBankApplication;
import com.georgeracu.mybank.MyBankConfiguration;
import com.georgeracu.mybank.bankaccount.api.response.BankAccountResponse;
import io.dropwizard.testing.ResourceHelpers;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.ws.rs.core.Response;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

/**
 * Author georgicaracu
 */
@Category(IntegrationTest.class)
@Ignore
public class BankAccountResourceITest {
    private static final String testConfigFile = "test-config.yml";

    private static final String CONFIG_PATH = ResourceHelpers.resourceFilePath(testConfigFile);

    @ClassRule
    public static final DropwizardAppRule<MyBankConfiguration> RULE = new DropwizardAppRule<>(
            MyBankApplication.class, CONFIG_PATH);

    @BeforeClass
    public static void migrateDatabase() throws Exception {
        RULE.getApplication().run("db", "migrate", ResourceHelpers.resourceFilePath(testConfigFile));
    }

    @AfterClass
    public static void tearDown() throws Exception {
        RULE.getApplication().run("db", "drop-all", "--confirm-delete-everything", ResourceHelpers.resourceFilePath(testConfigFile));
    }

    @Test
    public void shouldCreateAnAccount() {
        Response response = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/accounts").request().post(null);
        assertThat(response.getStatus(), is(201));
        assertNotNull(response.getLocation().getPath());
    }

    @Test
    public void shouldFindAccountByExternalId() {
        Response response = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/accounts").request().post(null);
        assertThat(response.getStatus(), is(201));
        String externalId = response.getLocation().getPath().replace("/accounts/", "");
        BankAccountResponse jsonResponse = RULE.client().target("http://localhost:" + RULE.getLocalPort() + "/accounts/" + externalId).request().get(BankAccountResponse.class);
        assertNotNull(jsonResponse);
        assertEquals(0, jsonResponse.getBalance());
    }
}
