package com.georgeracu.mybank;

import com.georgeracu.mybank.bankaccount.BankAccountResource;
import com.georgeracu.mybank.bankaccount.core.entity.BankAccountEntity;
import com.georgeracu.mybank.bankaccount.core.service.BankAccountService;
import com.georgeracu.mybank.bankaccount.db.BankAccountDAO;
import com.georgeracu.mybank.transfer.core.entity.TransferEntity;
import com.georgeracu.mybank.transfer.core.service.TransferService;
import com.georgeracu.mybank.transfer.db.TransferDAO;
import com.georgeracu.mybank.transfer.resources.TransferResource;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The entry point into this application
 * <p>
 * Author georgicaracu
 */
public class MyBankApplication extends Application<MyBankConfiguration> {
    private final Logger logger = LoggerFactory.getLogger(MyBankApplication.class);

    public static void main(String[] args) throws Exception {
        new MyBankApplication().run(args);
    }

    @Override
    public void initialize(Bootstrap<MyBankConfiguration> bootstrap) {
        bootstrap.addBundle(hibernateBundle);
        bootstrap.addBundle(migrationsBundle);
    }

    @Override
    public void run(MyBankConfiguration configuration, Environment environment) throws Exception {
        final BankAccountDAO bankAccountDAO = new BankAccountDAO(hibernateBundle.getSessionFactory());
        final BankAccountService bankAccountService = new BankAccountService(bankAccountDAO);
        final TransferDAO transferDAO = new TransferDAO(hibernateBundle.getSessionFactory());
        final TransferService transferService = new TransferService(transferDAO, bankAccountService);
        final TransferResource transferResource = new TransferResource(transferService);
        final BankAccountResource bankAccountResource = new BankAccountResource(bankAccountService);
        environment.jersey().register(transferResource);
        environment.jersey().register(bankAccountResource);
    }

    private final HibernateBundle<MyBankConfiguration> hibernateBundle =
            new HibernateBundle<MyBankConfiguration>(BankAccountEntity.class, TransferEntity.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(MyBankConfiguration configuration) {
                    return configuration.getDataSourceFactory();
                }
            };

    private final MigrationsBundle<MyBankConfiguration> migrationsBundle = new MigrationsBundle<MyBankConfiguration>() {
        @Override
        public DataSourceFactory getDataSourceFactory(MyBankConfiguration configuration) {
            return configuration.getDataSourceFactory();
        }
    };
}
