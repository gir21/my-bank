package com.georgeracu.mybank.transfer.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Author georgicaracu
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CreateTransferRequest {
    @JsonProperty(value = "sender")
    @NotNull
    private String sender;
    @JsonProperty(value = "receiver")
    @NotNull
    private String receiver;
    @Min(value = 1L)
    @JsonProperty(value = "amount")
    private long amount;

    public String getSender() {
        return sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public long getAmount() {
        return amount;
    }

    @Override
    public String toString() {
        return "CreateTransferRequest{" +
                "sender='" + sender + '\'' +
                ", receiver='" + receiver + '\'' +
                ", amount=" + amount +
                '}';
    }
}
