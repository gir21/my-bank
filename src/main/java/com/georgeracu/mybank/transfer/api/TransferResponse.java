package com.georgeracu.mybank.transfer.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.georgeracu.mybank.transfer.core.entity.TransferEntity;

import java.time.ZonedDateTime;

/**
 * Author georgicaracu
 */
public class TransferResponse {
    @JsonProperty("id")
    private String externalId;
    @JsonProperty("receiver")
    private String receiver;
    @JsonProperty("sender")
    private String sender;
    @JsonProperty("amount")
    private long amount;
    @JsonProperty("created_at")
    private ZonedDateTime createdAt;

    public TransferResponse() {
        // for JSON Deserialization
    }

    private TransferResponse(String externalId, String receiver, String sender, long amount, ZonedDateTime createdAt) {
        this.externalId = externalId;
        this.receiver = receiver;
        this.sender = sender;
        this.amount = amount;
        this.createdAt = createdAt;
    }

    public static TransferResponse from(TransferEntity transferEntity) {
        return new TransferResponse(transferEntity.getExternalId(),
                transferEntity.getReceiverId(),
                transferEntity.getSenderId(),
                transferEntity.getAmount(),
                transferEntity.getCreatedAt());
    }

    public String getExternalId() {
        return externalId;
    }

    public String getReceiver() {
        return receiver;
    }

    public String getSender() {
        return sender;
    }

    public long getAmount() {
        return amount;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }
}
