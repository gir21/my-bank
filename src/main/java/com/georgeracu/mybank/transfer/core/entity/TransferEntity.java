package com.georgeracu.mybank.transfer.core.entity;

import com.georgeracu.mybank.Random32CharactersStringGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;

/**
 * Author georgicaracu
 */
@Entity
@Table(name = "transfers")
public class TransferEntity implements Serializable {
    private static final long serialVersionUID = -158308464356906721L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "external_id")
    private String externalId;
    @Column(name = "sender_id")
    private String senderId;
    @Column(name = "receiver_id")
    private String receiverId;
    @Column(name = "amount")
    private long amount;
    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    public TransferEntity() {
        // JPA
    }

    public TransferEntity(String senderId, String receiverId, long amount) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.amount = amount;
        this.externalId = Random32CharactersStringGenerator.next();
        this.createdAt = ZonedDateTime.now();
    }

    public String getExternalId() {
        return externalId;
    }

    public long getId() {
        return id;
    }

    public String getSenderId() {
        return senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public long getAmount() {
        return amount;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TransferEntity that = (TransferEntity) o;
        return id == that.id &&
                Objects.equals(externalId, that.externalId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, externalId);
    }
}
