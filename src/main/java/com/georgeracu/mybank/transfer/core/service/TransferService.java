package com.georgeracu.mybank.transfer.core.service;

import com.georgeracu.mybank.bankaccount.core.service.BankAccountService;
import com.georgeracu.mybank.transfer.api.TransferResponse;
import com.georgeracu.mybank.transfer.core.entity.TransferEntity;
import com.georgeracu.mybank.transfer.db.TransferDAO;

import java.util.Optional;

/**
 * Author georgicaracu
 */
public class TransferService {

    private final TransferDAO dao;
    private final BankAccountService bankAccountService;

    public TransferService(TransferDAO dao, BankAccountService bankAccountService) {
        this.dao = dao;
        this.bankAccountService = bankAccountService;
    }

    public Optional<TransferResponse> createTransfer(String sender, String receiver, long amount) {
        bankAccountService.transferFunds(sender, receiver, amount);
        TransferEntity transfer = new TransferEntity(sender, receiver, amount);
        dao.save(transfer);
        return Optional.of(TransferResponse.from(transfer));
    }

    public Optional<TransferResponse> findByExternalId(String transferId) {
        return dao.findByExternalId(transferId).map(TransferResponse::from);
    }
}
