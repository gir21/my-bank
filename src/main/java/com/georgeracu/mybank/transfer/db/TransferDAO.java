package com.georgeracu.mybank.transfer.db;

import com.georgeracu.mybank.transfer.core.entity.TransferEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

/**
 * Author georgicaracu
 */
public class TransferDAO extends AbstractDAO<TransferEntity> {
    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public TransferDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public TransferEntity save(TransferEntity transfer) {
        return persist(transfer);
    }

    public Optional<TransferEntity> findByExternalId(String externalId) {
        String hql = "FROM TransferEntity t WHERE t.externalId = :externalId";
        Query query = currentSession().createQuery(hql);
        query.setParameter("externalId", externalId);
        List resultList = query.getResultList();
        if (resultList.size() == 1) {
            return Optional.of((TransferEntity) resultList.get(0));
        } else {
            return Optional.empty();
        }
    }
}
