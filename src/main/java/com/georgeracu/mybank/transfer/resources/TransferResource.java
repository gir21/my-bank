package com.georgeracu.mybank.transfer.resources;

import com.codahale.metrics.annotation.Timed;
import com.georgeracu.mybank.transfer.api.CreateTransferRequest;
import com.georgeracu.mybank.transfer.core.service.TransferService;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Author georgicaracu
 */
@Path("/transfers")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TransferResource {
    private final Logger logger = LoggerFactory.getLogger(TransferResource.class);

    private final TransferService service;

    public TransferResource(TransferService service) {
        this.service = service;
    }

    @POST
    @Timed
    @UnitOfWork
    public Response createTransfer(@NotNull @Valid CreateTransferRequest request) {
        logger.info("Received a request to create a Transfer [{}]", request);
        return service.createTransfer(request.getSender(), request.getReceiver(), request.getAmount()).map(created -> {
            URI location = UriBuilder.fromResource(TransferResource.class).path("/" + created.getExternalId()).build();
            return Response.created(location).entity(created).build();
        }).orElseGet(() -> Response.status(500).entity("There was a downstream error").build());
    }

    @GET
    @Path("/{id}")
    @Timed
    @UnitOfWork
    public Response getTransferById(@PathParam("id") String transferId) {
        logger.info("Received request to get a Transfer [{}]", transferId);
        return service.findByExternalId(transferId).map(response -> Response.ok().entity(response).build())
                .orElseGet(() -> Response.status(404).build());
    }
}
