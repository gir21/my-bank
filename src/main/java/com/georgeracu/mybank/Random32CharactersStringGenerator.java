package com.georgeracu.mybank;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Helper class that generates 32 character long strings.
 * Should use enough entropy that chances for collisions are
 * extremely low
 * <p>
 * Author georgicaracu
 */
public class Random32CharactersStringGenerator {
    public static String next() {
        int tokenLength = 20;
        SecureRandom secureRandom = new SecureRandom();
        byte[] token = new byte[tokenLength];
        secureRandom.nextBytes(token);
        return new BigInteger(1, token).toString(32);
    }
}
