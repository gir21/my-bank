package com.georgeracu.mybank.bankaccount.db;

import com.georgeracu.mybank.bankaccount.core.entity.BankAccountEntity;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

/**
 * Author georgicaracu
 */
public class BankAccountDAO extends AbstractDAO<BankAccountEntity> {
    /**
     * Creates a new DAO with a given session provider.
     *
     * @param sessionFactory a session provider
     */
    public BankAccountDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public Optional<BankAccountEntity> findByExternalId(String externalId) {
        String hql = "FROM BankAccountEntity b WHERE b.externalId = :externalId";
        Query query = currentSession().createQuery(hql);
        query.setParameter("externalId", externalId);
        List resultList = query.getResultList();
        if (resultList.size() == 1) {
            return Optional.of((BankAccountEntity) resultList.get(0));
        } else {
            return Optional.empty();
        }
    }

    public BankAccountEntity save(BankAccountEntity newAccount) {
        return persist(newAccount);
    }
}
