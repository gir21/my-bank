package com.georgeracu.mybank.bankaccount;

import com.codahale.metrics.annotation.Timed;
import com.georgeracu.mybank.bankaccount.api.response.BankAccountResponse;
import com.georgeracu.mybank.bankaccount.core.service.BankAccountService;
import io.dropwizard.hibernate.UnitOfWork;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;

/**
 * Author georgicaracu
 */
@Path("/accounts")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class BankAccountResource {

    private static final Logger logger = LoggerFactory.getLogger(BankAccountResource.class);
    private final BankAccountService service;

    public BankAccountResource(BankAccountService service) {
        this.service = service;
    }

    @POST
    @Timed
    @UnitOfWork
    public Response create() {
        logger.info("Received a POST request to create an account");
        return service.create().map(
                this::getResponseCreates)
                .orElseGet(() -> Response.status(500).build());
    }

    @GET
    @Path("/{id}")
    @Timed
    @UnitOfWork
    public Response findByExternalId(@PathParam("id") @NotNull String externalId) {
        logger.info("Received a GET request to find by externalId [{}]", externalId);
        return service.findByExternalId(externalId).map(response -> Response.ok().entity(response).build())
                .orElseGet(() -> Response.status(404).build());
    }

    private Response getResponseCreates(BankAccountResponse response) {
        URI build = UriBuilder.fromResource(BankAccountResource.class).path("/" + response.getId()).build();
        logger.info("Responding with created [{}]", response);
        return Response.created(build).entity(response).build();
    }
}
