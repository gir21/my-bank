package com.georgeracu.mybank.bankaccount.core.exception;

import javax.ws.rs.WebApplicationException;

import static java.lang.String.format;

/**
 * Author georgicaracu
 */
public class InvalidAmountException extends WebApplicationException {
    public InvalidAmountException(long amount) {
        super(format("Invalid value for amount [%s]", amount));
    }

    public InvalidAmountException(String message) {
        super(message);
    }
}
