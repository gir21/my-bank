package com.georgeracu.mybank.bankaccount.core.exception;

import javax.ws.rs.WebApplicationException;

/**
 * Author georgicaracu
 */
public class InsufficientFundsException extends WebApplicationException {
    public InsufficientFundsException(String message) {
        super(message);
    }
}
