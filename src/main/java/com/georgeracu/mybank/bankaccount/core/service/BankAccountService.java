package com.georgeracu.mybank.bankaccount.core.service;

import com.georgeracu.mybank.bankaccount.api.response.BankAccountResponse;
import com.georgeracu.mybank.bankaccount.core.entity.BankAccountEntity;
import com.georgeracu.mybank.bankaccount.core.exception.BankAccountNotFoundException;
import com.georgeracu.mybank.bankaccount.db.BankAccountDAO;

import java.util.Optional;

/**
 * Author georgicaracu
 */
public class BankAccountService {
    private final BankAccountDAO dao;

    public BankAccountService(BankAccountDAO dao) {
        this.dao = dao;
    }

    public void transferFunds(String sender, String receiver, long amount) {
        dao.findByExternalId(sender).map(senderAccount ->
                dao.findByExternalId(receiver).map(receiverAccount -> {
                    senderAccount.debit(amount);
                    receiverAccount.credit(amount);
                    return receiver;
                }).<BankAccountNotFoundException>orElseThrow(() -> new BankAccountNotFoundException(receiver))
        ).orElseThrow(() -> new BankAccountNotFoundException(sender));
    }

    public Optional<BankAccountResponse> create() {
        BankAccountEntity entity = dao.save(BankAccountEntity.defaultAccount());
        return Optional.of(BankAccountResponse.from(entity));
    }

    public Optional<BankAccountResponse> findByExternalId(String externalId) {
        return dao.findByExternalId(externalId).map(BankAccountResponse::from);
    }
}
