package com.georgeracu.mybank.bankaccount.core.entity;

import com.georgeracu.mybank.Random32CharactersStringGenerator;
import com.georgeracu.mybank.bankaccount.core.exception.InsufficientFundsException;
import com.georgeracu.mybank.bankaccount.core.exception.InvalidAmountException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.ZonedDateTime;
import java.util.Objects;

import static java.lang.String.format;

/**
 * Author georgicaracu
 */
@Entity
@Table(name = "bank_accounts")
public class BankAccountEntity {
    @Id()
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;
    @Column(name = "external_id")
    private String externalId;
    @Column(name = "units")
    private long units;
    @Column(name = "created_at")
    private ZonedDateTime createdAt;

    public BankAccountEntity() {
        // JPA
    }

    private BankAccountEntity(long units) {
        this.units = units;
        this.externalId = Random32CharactersStringGenerator.next();
        this.createdAt = ZonedDateTime.now();
    }

    public long getBalance() {
        return units;
    }

    public String getExternalId() {
        return externalId;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    public long getId() {
        return id;
    }

    public void credit(long amount) {
        if (amount > 0) {
            units = units + amount;
        } else {
            throw new InvalidAmountException(amount);
        }
    }

    public void debit(long amount) {
        if (amount < 1) {
            throw new InvalidAmountException(format("Negative withdraw amounts are not allowed [%s]", amount));
        } else if (units >= amount) {
            units = units - amount;
        } else {
            throw new InsufficientFundsException(format("Insufficient funds available to debit [%s]", amount));
        }
    }

    public static BankAccountEntity defaultAccount() {
        return new BankAccountEntity(0L);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccountEntity entity = (BankAccountEntity) o;
        return Objects.equals(externalId, entity.externalId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(externalId);
    }
}
