package com.georgeracu.mybank.bankaccount.core.exception;

import javax.ws.rs.WebApplicationException;

import static java.lang.String.format;

/**
 * Author georgicaracu
 */
public class BankAccountNotFoundException extends WebApplicationException {
    public BankAccountNotFoundException(String accountId) {
        super(format("Bank account not found for id [%s]", accountId), 404);
    }
}
