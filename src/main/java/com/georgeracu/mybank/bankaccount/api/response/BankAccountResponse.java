package com.georgeracu.mybank.bankaccount.api.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.georgeracu.mybank.bankaccount.core.entity.BankAccountEntity;

import java.time.ZonedDateTime;

/**
 * Author georgicaracu
 */
public class BankAccountResponse {
    @JsonProperty("id")
    private String id;
    @JsonProperty("balance")
    private long balance;
    @JsonProperty("created")
    private ZonedDateTime createdAt;

    public BankAccountResponse() {
        // for JSON Deserialization
    }

    private BankAccountResponse(String id, long balance, ZonedDateTime createdAt) {
        this.id = id;
        this.balance = balance;
        this.createdAt = createdAt;
    }

    public static BankAccountResponse from(BankAccountEntity entity) {
        return new BankAccountResponse(entity.getExternalId(), entity.getBalance(), entity.getCreatedAt());
    }

    public String getId() {
        return id;
    }

    public long getBalance() {
        return balance;
    }

    public ZonedDateTime getCreatedAt() {
        return createdAt;
    }

    @Override
    public String toString() {
        return "BankAccountResponse{" +
                "id='" + id + '\'' +
                ", balance=" + balance +
                ", createdAt=" + createdAt +
                '}';
    }
}
