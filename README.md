# My Bank

A Java application built using Dropwizard framework

## Intro

### Requirements
* Design and implement a RESTful API (including data model and the backing implementation)
for money transfers between accounts.
* The app should be self runnable
* The data store should be in memory
* The app should be tested

#### Implementation
* This app is created using Dropwizard 1.3.5, a minimalistic API framework. The app is coded in Java.
* It uses Maven for packaging

#### How to build it using Maven
`mvn clean install`

#### How to run the JAR
`java -jar target/mybank-1.0-SNAPSHOT.jar server config.yml`
 
#### Dependencies
* Dropwizard 1.3.5
* JUnit 4.12
* Mockito 2.23.4
* H2 1.4.197

## Application

### Resources

| Path                  | HTTP Type | Description           |
|---                    |---        |---                    |
| `/transfers`          | POST      | Create a new transfer |
| `/transfers/{id}`     | GET       | Get an existing transfer |
| `/accounts`           | POST      | Create a new account  |
| `/accounts/{id}`      | GET       | Get and existing account by externalId |

#### Transfers

##### Create Request with POST to `/accounts`
```sh
curl --header "Content-Type: application/json" \
  --request POST \
  --data '{"sender":"abd","receiver":"xyz","amount":100}' \
  http://localhost:8080/transfers
```
This is used to create a new transfer. The transfer object is used to represent
an interaction between two existing accounts.
For this request to be valid, it requires both accounts to exist and the amount 
to be greater than zero.
```json
{
  "sender": "sender-id",
  "receiver": "receiver-id",
  "amount": "1000"
}
```

##### Response
* 201.Created with Location header set to `/transfers/transfer-abc`
* 422.Unprocessable Entity if the request is not valid
* 404.Not Found if any of the two accounts don't exist
* 500.Internal Server Error if the sender does not have enough 
funds to be transferred or anything happens that throws a WebApplicationException

##### Entity
| Field         | Type          | Generated | Size
|---            |---            |---
| id            | long          | By the db | Long.MAX_VALUE
| externalId    | String        | Random at creation | 32 characters
| senderId      | String        | By the Bank Account |
| receiverId    | String        | By the Bank Account |
| amount        | long          | By the transfer request | From 1 to Long.MAX_VALUE
| createdAt     | ZonedDateTime | At time of creation |

#### Bank Account
A bank account is a concept used to hold a value (units) representing
the smallest unit of a currency. Datatype used is long. It has methods
to debit (withdraw, remove) units and to credit (deposit, add) units.

##### Constraints
* A new account starts with zero funds
* To have a trace of all the funds created, and because there is no outside method
to add funds, the app starts with a master account, that has lost of money
* Credit supports only positive values, otherwise throws InvalidAmountException
* Debit supports only positive values, otherwise throws InvalidAmountException
* Debit will throw an InsufficientFundsException if there are not enough
funds available

##### Entity
| Field         | Type          | Generated                 | Size
|---            |---            |---                        |---
| id            | long          | By the database strategy  | Long.MAX_VALUE
| externalId    | String        | Random at creation        | 32 characters
| units         | long          | Starts with zero and changes via credit/debit   | From 1 to Long.MAX_VALUE
| createdAt     | ZonedDateTime | Timestamp when created    | Default |

##### Create a Bank Account with POST to `/accounts`
It requires an POST request to create a default account with zero units.

###### Request 
`curl -X POST http://localhost:8080/accounts`

###### Response 
201.Created and header Location set as `/accounts/account-id`
```json
{
  "id":"jfoukgf2iv28dfhsjnuir4veo4563gq8",
  "balance":0,
  "created":1544725559.257000000
}
```
##### Get a Bank Account with GET to `/accounts/{id}`

###### Response
200.OK with 
```json
{
  "id": "jfoukgf2iv28dfhsjnuir4veo4563gq8",
  "balance": 100,
  "created_at": "1544725559.257000000"
}
```

404.Not Found

### Usage

#### Initial account
The application starts with a master account. 
In this case the id is hardcoded as 
`aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa`. From this account transfers will
originate and therefore they can be traced.

#### How to use the app

##### Build the app using Maven
* `mvn clean install -DskipTests` will create an executable jar
* `java -jar target/mybank-1.0-SNAPSHOT.jar db migrate config.yml` will run the migrations
and it will generate the tables and the initial master bank account
* `java -jar target/mybank-1.0-SNAPSHOT.jar server config.yml` will start the application. 
Follow the logs for the started message. It should take a few seconds to start

##### Make API calls using an HTTP client eg. Postman, curl
* Create an account `curl -X POST http://localhost:8080/accounts`
* Get the id of the newly created account
* Make a transfer from the master account `POST http://localhost:8080/transfers`
and the payload

#### Types of possible exceptions
* Runtime exceptions at startup might be due the migrations not being run
* HTTP 400.Bad Request - some parameters are missing from your request/payload
* HTTP 404.Not Found - either entity not found (account, transfer) or URL not found
* HTTP 422.Unprocessable Entity - your payload could not be mapped to an object due to 
malformed/missing fields
* HTTP 500.Internal Server Error - you might not ave funds available for transfer, or something
really bad happened. If is a custom 500 error you will see a meaningful message 

#### Further work
* Add resources to list all the accounts/transfers
* Add currency to the accounts
* Add more real world concepts to the account: BIC, SWIFT etc
